:LaTeX_PROPERTIES:
#+LANGUAGE:              de
#+OPTIONS:     		 d:nil todo:nil pri:nil tags:nil
#+OPTIONS:	         H:4
#+LaTeX_CLASS: 	         orgstandard
#+LaTeX_CMD:             xelatex
:END:
:REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: league
#+REVEAL_EXTRA_CSS: ./mystyle.css
#+REVEAL_HLEVEL: 1
#+OPTIONS: timestamp:nil toc:nil num:nil
:END:

#+TITLE: Übertragungsmedien
#+SUBTITLE: ITT-Netzwerk
#+AUTHOR: Sebastian Meisel

* Eigenschaften von Übertragungsmedium

** Bandbreite

  Die *Bandbreite* sagt aus, wie viele /Datenbits/ über eine Netzwerkverbindung in einer
  Sekunde übertragen werden kann.

  In Netzwerken sind Kilo- Mega- oder Gigabit pro Sekunde als Einheiten üblich. 

  Im wesentlichen hängt die Bandbreite von der Übertragungs-/Frequenz/ und der Anzahl der
  Übertragungs-/kanäle/, z. B. die Anzahl der Leitungen.

#+BEGIN_NOTES
 Die /maximale/ *Bandbreite* hängt zudem von der /Reichweite/ ab, über die die Signale
 übertragen werden sollen und der /Dämpfung/.
#+END_NOTES 

** Reichweite

 In den /Protokollen/ zur Datenübertragung wird bestimmt, wie weit ein /Signal/ in einer
 bestimmten /Bandbreite/ verlustfrei übertragen werden kann. Während bei /kabelgebundenen/
 Medien vor allem die Eigenschaften des Kabels die *Reichweite* bestimmen, gibt es bei
 /kabellosen/ Verbindungen in der Regel viele Faktoren, die die *Reichweite* beeinflussen. 

#+BEGIN_NOTES
 Die können Hindernisse, Störfrequenzen, aber auch die Luftfeuchtigkeit sein.
#+END_NOTES



** Frequenz
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Frequenz.png]]

 Signale durch die sogenannte /Modulation/ eines /Trägersignals/ übertragen.
#+BEGIN_NOTES
 D. h. das bestimmte Eigenschaften verändert werden.
#+END_NOTES
 Das /Trägersignal/ ist dabei entweder eine Sinuswelle oder eine Folge von Impulsen, wobei
 ein Strom oder Lichtsignal an und ausgeschaltet wird. Die *Frequenz* wird in (Mega-, Kilo-,
 Giga-)Hertz gemessen und gibt an, wie oft die Welle in einer Sekunde hin und her
 schwingt, bzw. wie viele Ein-/Ausschaltvorgänge in einer Sekunde geschehen. 

#+BEGIN_NOTES
#+CAPTION: Digitales Signal
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Digitales_Signal.png]]  

 Je höher die *Frequenz* desto mehr Schwingungen. Die Bandbreite hängt dann davon ab, wie
 hoch die Frequenz ist, und wie viele Schwingungen für die Übertragung eines /Bits/ notwendig
 sind. 
#+END_NOTES
 

** Dämpfung
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Dämpfung.png]]
 
 Das Trägersignal hat neben der /Frequenz/ auch eine /Signalstärke/. Mit der Zeit / Distanz nimmt die
 /Signalstärke/ ab, weil Energie "verloren" geht. Irgendwann ist sie so schwach, dass die
 Signale nicht mehr (verlässlich) erkannt werden können.

#+BEGIN_NOTES
 Energie kann niemals wirklich verloren gehen, sie wird aber zum Beispiel in Wärme
 umgewandelt. 
#+END_NOTES

 Wie schnell die /Signalstärke/ abnimmt wird mit der Dämpfung angegeben. Je höher die Dämpfung
 desto kurzer die /Reichweite/. 

#+BEGIN_NOTES
 Die Dämpfung wird in /Dezibel (Db)/ angegeben. Diese Skala ist nicht linear sondern
 logarithmisch, d .h. dass anfangs kleine Erhöhungen der Dezibel sich relativ stark
 auswirken, dann aber immer weniger.

 Bei etwa 6 Db hat sich die Ausgangsleistung gegenüber der Eingangsleistung halbiert. Bei
 20 Db sind doch etwa 10 % Leistung vom Ausgangssignal übrig.   
#+END_NOTES


** Duplexfähigkeit

 Eine wichtige Eigenschaft ist auch die Frage in welche Richtung Daten übertragen werden
 können. Man unterscheidet dabei zwischen:
 
#+ATTR_REVEAL: :frag (appear)
 - *Simplex (SX)* oder Richtungsbetrieb, wobei Daten nur in eine Richtung übertragen werden
   können. Dies gibt es in der Netzwerktechnik nur in Form von: 
#+ATTR_REVEAL: :frag (appear)
   - *Dual-Simplex (DSX)* wobei zwei getrennte Leitungen für  das Senden und Empfangen von
     Daten genutzt werden. 
#+ATTR_REVEAL: :frag (appear)
 - *Halbduplex (HX/HDX)* oder Wechselbetrieb: Daten werden abwechselnd gesendet und
   empfangen.
 - *Vollduplex (DX/FDX)* oder Gegenbetrieb: Daten werden gleichzeitig in beide Richtungen übertragen.

** Störsicherheit

 Bei der /Signalübertragung/ gibt es verschiedene Signale, die zu *Störungen* führen
 können. So können sich elektromagnetischen Felder überlagern, Signale können reflektiert
 werden, Hindernisse können Funksignal stören, etc.

 Die *Störsicherheit* beschreibt, wie gut eine /Medium/ gegen solche Störungen /abgeschirmt/ ist.


* Kabelgebundene Medien

#+ATTR_REVEAL: :frag (appear)
 + Vorteile (gegenüber kabellosen):
   - Bessere Abhörsicherheit.
   - Geringerer Energieverbrauch.
   - Höhere Störsicherheit.

** Kupferkabel

 Kupfer ist das am häufigsten genutzte Übertragungsmedium. Kupfer ist hochverfügbar, ist
 ein guter Leiter und vergleichsweise flexibel.

 Es gibt verschiedene Verarbeitungen des Materials als Kabel.

#+BEGIN_NOTES
 Bei Kupferkabeln spielen noch zwei Eigenschaften eine Rolle:

 - *LSZH* (Low smoke zero halogen) oder *LS0H* sind halogenfreie Kabel, die bei einen Kabel
   oder Gebäudebrand weniger giften Rauch und vor allem eben kein Halogene abgeben,
   dass besonders gesundheits- und umweltschädlich ist. 
 - *CCA* (Copper Clad Aluminium ) statt 100% Kupferkabel sind eigentlich Aluminiumkabel, die
   nur mit Kupfer umhüllt sind. Sie nutzte die Tatsache aus, dass Elektronen sich in Kabeln
   hauptsächlich an der Oberfläche bewegen. Sie sind billiger, aber
   - weniger flexibel.
   - brechen leichter.
   - haben eine höhere Dämpfung. 
#+END_NOTES


*** Koaxial 

#+CAPTION: Koaxialkabel
  #+ATTR_HTML: :width 50%
  #+ATTR_LATEX: :width .65\linewidth
  #+ATTR_ORG: :width 700
  [[file:Bilder/Koax-Kabel.png]]

#+BEGIN_NOTES

 Während Koaxialkabel anfangs das meist genutzte /Übertragsmedium/ waren, werden sie heute
 kaum noch genutzt.

 Ein zentrales Kupferkabel ist umgeben von einem Isolator, der von einem /Flechtschirm/
 umgeben ist. Ohne diesen wäre das Kabel eine Antenne, was zu elektromagnetischen
 Störungen führen würde. Ganz außen wird es noch mit einem Gummi-, Kunststoff oder
 Teflon-Mantel geschützt.  

 + Vorteile:
   - Günstig.
   - Leicht.
   - Biegsam.
 + Nachteile
   - Nur ein Kabel: Begrenzte Bandbreite.

 - *Thicknet* (IEEE 802.3 Clause 8), auch *Yellow Cable*:
   - 10 Mbit/s über 500m.
   - Halbduplex.
   - Vollduplex.
   - Sehr störsicher.
   - geringe Dämpfung.

 - *Thinnet* (IEEE 802.3 Clause 10):
   - Reichweite 185m
   - 0,5 cm Durchmesser,
   - Anbindung an Computer über T-Stück.

#+END_NOTES


*** Twisted Pair
#+CAPTION: S-FTP Twisted-Pair-Kabel
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/S-FTP-Kabel_3D.png]]

*Twisted-Pair-Kabel* sind die am häufigsten eingesetzten Kabel in der Netzwerktechnik. Die
allgemein bekannten Cat-Kabel mit RJ45-Stecker setzen alle auf diese Technik.

#+BEGIN_NOTES
 
 Um höhere *Bandbreiten* zu erreichen, drängt sich der Gedanke auf, mehrere Kabel zu
 verwenden. Allerdings würden sich mehrere /Koaxialkabel/ als Adern im selben Kabel verlegt werden gegenseitig stören.  

 Verdrillt man aber /zwei Adern/ miteinander werden diese /Störungen/ stark minimiert. 

 Zwar erreicht man nicht dieselbe /Reichweite/, wie bei /Koaxialkabeln/ - die Reichweite von
 *Twisted-Pair-Kabeln* liegt bei maximal 100 m, gegenüber bis zu 500 m bei /Koaxialkabeln/ -
 dafür erreicht man aber eben deutlich höhere Übertragungsraten.

 Allerdings nehmen die /Störungen/ mit höheren /Frequenzen/ zu, sodass zusätzliche Maßnahmen
 nötig sind, um eine ausreichende /Reichweite/ zu gewährleisten.

 In der Regel werden 4 /Adernpaare/ verlegt.

#+END_NOTES



**** UTP: Unshielded Twisted Pair
#+CAPTION: UTP-Kabel
#+ATTR_HTML: :width 30%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/UTP-Kabel.png]]

Einfachste Form eines *Twisted-Pair-Kabels*.


**** STP Shielded Twisted Pair
#+CAPTION: STP-Kabel
#+ATTR_HTML: :width 30%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/STP-Kabel.png]]

Gegenüber dem /UTP-Kabel/ hat das *STP*-Kabel um jedes /Adernpaar/ einen Folienschirm, der
verhindert, dass sich die /Adernpaare/ gegenseitig stören. 

**** SUTP Screened-Unshielded Twisted Pair
#+CAPTION: S/UTP-Kabel
#+ATTR_HTML: :width 30%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/SUTP-Kabel.png]]
 
 Gegenüber dem  /UTP/-Kabel verfügt das *S/UTP*-Kabel über einer Abschirmung um das
 gesamte Kabel, sodass weniger abstrahlt und vor /Störungen/ von außen gesichert ist. 

#+BEGIN_NOTES
 Dieser Außenschirm ist wie beim /Koaxialkabel/ in der Regel ein Metallnetz. 
#+END_NOTES


**** S/FTP Screened-Foiled Twisted Pair
#+CAPTION: S/FTP-Kabel
#+ATTR_HTML: :width 30%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/S-FTP-Kabel.png]]

 Das *S/FTP*-Kabel verbindet die Eigenschaften von /STP/- und /S/UTP/-Kabel, verfügt also über
 einen Adern- und einem Gesamtschirm. 

#+BEGIN_NOTES
 Dadurch sind höhere /Frequenzen/ und damit eine höhere /Bandbreite/ möglich, die
 zusätzlichen Schilde mache das Kabel aber auch steifer, sodass es sich nicht so leicht
 verlegen lässt.
#+END_NOTES

*** CAT Kabel mit RJ45-Stecker

 Zur Zeit sind CAT-Kabel mit dem RJ45-Stecker die wichtigsten Kabel im Netzwerkbereich,
 weshalb wir sie gesondert betrachten.

#+CAPTION: Pinbelegung auf RJ45-Steckern
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/RJ45.png]]

 


** Glasfaser

 In *Glasfaser*-Kabeln werden Signale mit Lasern oder LED-Licht im inneren einer sehr dünnen *Glas*-,
 seltener einer *Kunststoff*-Röhre. 

#+ATTR_REVEAL: :frag (appear)
 + Vorteile:
#+ATTR_REVEAL: :frag (appear)
   - hohe Bandbreite. 
   - sehr hohe Störsicherheit (keine elektromagnetische Beeinflussung).
   - hohe Abhörsicherheit.
   - Geringer Energieverbrauch.
   - Hohe Reichweite (abhängig von der Kabelart).
#+ATTR_REVEAL: :frag (appear)
 + Nachteile:
#+ATTR_REVEAL: :frag (appear)
   - teure Anschaffung.
   - nicht sehr flexibel, zerbrechlich (außer Kunststoffkabel).
   - In der Regel nur /dual-simplex/-fähig. 

*** Multimode

 *Multimode*-Glasfasern übertragen gleichzeitig mehrere Signale (*Moden*). Dies klingt nach
 einem Vorteil, allerdings sind dies die (vergleichsweise) billigeren Kabel. In einem
 Kern mit einem vergleichsweise großen /Kern/ von *50 µm* werden die Laser in einem Winkel
 eingestrahlt und reflektiert. 

**** mit Stufenindex
#+CAPTION: Multimode Glasfaser mit Stufenindex
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Multimode__SI_Fiber.png]]

 In *Multimode*-Glasfasern mit *Stufenindex* werden die Lichtstrahlen hart am Kernrand reflektiert.
 Das führt dazu, dass die Signale verschiedene Laufzeiten haben.

#+BEGIN_NOTES
  - Vorteil:
    - Günstige Herstellung.
  - Nachteile (Im Vergleich zu anderen Glasfasern):
    - geringe Bandbreite.
    - mittlere Dämpfung.
    - geringe Reichweite.

 Die /Bandbreite/ ist immer noch höher als bei Kupferkabeln. 
#+END_NOTES

**** mit Gradientindex
#+CAPTION: Multimode Glasfaser mit Gradientindex
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Multimode__GI_Fiber.png]]
 
 Die Wand des Kerns ist speziell gearbeitet, sodass das Licht weich in einer Kurve
 (*Gradient*) reflektiert wird, sodass alle Signale (fast) die selbe Laufzeit haben.

#+BEGIN_NOTES
 - Vorteil (gegenüber Stufenindex):
   - hohe Bandbreite.
   - geringe Dämpfung.
   - kaum /Modendispersion/ = Signal haben fast gleiche Laufzeit.
#+END_NOTES
 

*** Single Mode 
#+CAPTION: Single Mode Glasfaser
#+ATTR_HTML: :width 50%
#+ATTR_LATEX: :width .65\linewidth
#+ATTR_ORG: :width 700
[[file:Bilder/Single_Mode_Fiber.png]]

 Bei der *Mono-* oder *Single-Mode*-Glasfaser wird ein Laser parallel zum Kern eingespeist. Der Kern hat
 einen sehr kleinen Durchmesser von *9 µm*. Das Kabel muss absolut gerade verlegt werden,
 sodass es im Kern nicht zu Reflexionen kommt. 

#+BEGIN_NOTES
 - Vorteile:
   - sehr hohe Bandbreite.
   - fast keine Dämpfung.
   - sehr hohe Reichweite.
 - Nachteile:
   - sehr teuer in Herstellung.
   - aufwendige Verlegung.

 Diese Technik wird vor allem im *Backbone* des Internets also die zentralen
 Langstreckenverbindungen genutzt. 
#+END_NOTES
 

* Kabellose Medien

 Vorteile:
  - Keine Baumaßnahmen notwendig.
  - Überall einsetzbar.
  - Kostengünstig.
 Nachteile:
  - Störanfällig.
  - Leichter abhörbar.
  - Höher Energieverbrauch. 

*** Funk
  
  Übertragung von Signalen durch nicht gerichtete elektromagnetische Wellen. Dies breiten
  sich von der Sendeantennen in alle Richtungen kreisförmig aus.

  - Vorteil:
    - empfangende Geräte können sich frei im Empfangsbereich bewegen.
  - Nachteile:
    - Hohe Störanfälligkeit.
    - Besonders leicht abhörbar.
    - Hoher Energieverbrauch.

**** WLAN
    
    *WLAN* ist eine Umsetzung des Ethernetprotokolls über ungerichtete
    Funkverbindungen. 

    Es ist in der /IEEE 802.11/ normiert, zu der es diverse Erweiterungen gibt.
    Die neueste Version /WiFi 6/6E/ (IEEE 802.11ax) könnte bis zu ca. 9 Gbs übertragen. In
    der Praxis sind maximale Geschwindigkeiten bis ca. 1.2 Gbs realistischer.

#+BEGIN_NOTES
 | WLAN-Generation                  | Wi-Fi 4      | Wi-Fi 5       | Wi-Fi 6 / 6E        |
 | IEEE-Standard                    | IEEE 802.11n | IEEE 802.11ac | IEEE 802.11ax       |
 |----------------------------------+--------------+---------------+---------------------|
 | Maximale Übertragungsrate[fn:1]     | 600 MBit/s   | 6.936 MBit/s  | 9.608 MBit/s        |
 | Theoretische Übertragungsrate[fn:2] | 300 MBit/s   | 867 MBit/s    | 1.200 MBit/s        |
 | Maximale Reichweite              | 100 m        | 50 m          | 50 m                |
 | Frequenzbereich                  | 2,4 + 5 GHz  | nur für 5 GHz | 2,4 + 5 GHz + 6 GHz |
 | Maximale Sende/Empfangseinheiten | 4 x 4        | 8 x 8         | 8 x 8               |
 | Antennentechnik                  | MIMO         | (MU-MIMO)     | MU-MIMO             |
 | Maximale Kanalbreite             | 40 MHz       | 160 MHz       | 160 MHz             |
 | Modulationsverfahren             | 64QAM        | 256QAM        | 1024QAM             |

#+END_NOTES

   

**** Mobile Netze

    * (Erste Generation)
      - Advanced Mobile Phone Service (AMPS)
    * Zweite Generation (2G):
      - Global System for Mobile 13 Kbs 
        - GPRS (G): 53,2 Kbs
	- EDGE (E): 256 Kbs
    * Dritte Generation (2G)
      - Universal Mobile Telecommunications System (UMTS): 384 Kbs
	- HSDPA (H/3,5G/3G+): 7,2 Mbs
	- HSDPA+ (H+): 42 Mbs
    * Vierte Generation (4G)
      - Long Term Evolution (LTE): 500 Mbs
      - LTE-A (LTE+, 4G+): 1 Gbs
    * 5G: bis 10 Gbs

#+BEGIN_NOTES
  Die sogenannten Generationen fassen jeweils verschiedene - zum Teil konkurrierende oder
  nur lokal genutzte -  Standards zusammen.
  
  Zu vielen Standards gibt es zudem Weiterentwicklungen und Erweiterungen. So basieren /GPRS/
  und /EDGE/ auf /GSM/. 

  5G widerum baut auf /LTE/ auf.
#+END_NOTES 

**** Weitere Beispiele:
     
     - Bluetooth.
    #+BEGIN_NOTES
        *Bluetooth* dient zur Herstellung von /Ad-hoc/-Netzwerken zwischen Geräten über kurze
        Distanzen.
        
       | Version | Leistung | Datenrate         | Reichweite (Innen/Außen) |
       |---------+----------+-------------------+--------------------------|
       | 1 / 1.2 | 1 mW     | 732,2 Kbs / 1 Mbs | 1 / 10 m                 |
       |---------+----------+-------------------+--------------------------|
       |       2 | 1 mW     | 2,1 Mbs           | 1 / 10 m                 |
       |---------+----------+-------------------+--------------------------|
       |       3 | 2,5 mW   | 24 Mbs            | 10 / 50 m                |
       |---------+----------+-------------------+--------------------------|
       | 4 / 4.2 | 2,5 mW   | 25 / 26 Mbs       | 10 / 50 m                |
       |---------+----------+-------------------+--------------------------|
       |       5 | 100 mW   | 50 Mbs            | 40 / 200 m               |
       #+TBLFM: 
       
       Seit Version 4 gibt es eine Low-Energy-Spezifikation.   
       #+END_NOTES
 
     - Near Field Communications (NFC)
    #+BEGIN_NOTES
       *NFC* dienst zum Auslesen von /RFID/ Chips. Diese werden über das Funksignal auf sehr kurze
        Distanz sowohl mit Energie versorgt, als auch Ausgelesen. Außerdem können damit Daten
        zwischen zwei Geräten ausgetauscht werden.
      
       Ein /RFID-Chip/ besteht aus einer Antenne/Induktionsspule und einem kleinen Chip.
      
      #+CAPTION: [[https://commons.wikimedia.org/w/index.php?curid=7438963][RFID . von Kalinko, CC BY-SA 3.0]]
      #+ATTR_HTML: :width 50% :alt "RFID-Chip mit Induktionsantenne"
      #+ATTR_LATEX: :width .65\linewidth
      #+ATTR_ORG: :width 700
      [[file:Bilder/RFID-Chip.jpg]]  
      #+END_NOTES
 
     - ZigBee.
     - Matter
#+BEGIN_NOTES
 *ZigBee* und *Matter* sind Beispiele für Netzwerkstandards, die  vor allem für die Vernetzung
  von /Internet of Things (IoT)/-Hardware. Sie werden hauptsächlich in der /Home Automation/,
  also zur Vernetzung und Steuerung von Geräten, wie Smart-Steckdosen, ~Lichtern,
  ~Thermostaten, etc. genutzt. Sie sind wie Bluetooth auf kurze Entfernungen ausgelegt,
  verbrauchen aber weniger Energie.

 *Matter* soll dabei als gemeinsamer Standard die Geräte vieler verschiedener Hersteller
  verknüpfen, die bisher auf eigene Netzwerkstandard setzen.
#+END_NOTES
 
     - LoRaWAN.
#+BEGIN_NOTES
 *Low Range Wide Area Network (LoRaWAN)* ist ein Netzwerkstandars, der es vor allem
  Sensortechnik erlauben soll Daten mit geringer Bandbreite (*0,3 – 50 Kbs*) über große
  Entfernungen (*> 10km*) auszutauschen.
#+END_NOTES


*** Richtfunk

  Übertragung von Signalen durch gerichtete elektromagnetische Wellen. Diese werden durch
  spezielle /Richtannten/ auf ein ausgewähltes Ziel ausgerichtet.

  - Vorteile (gegenüber ungerichtetem Funk):
    - Hohe Bandbreite.
    - Große Reichweite.
    - Abhör- und störsicherer.

#+BEGIN_NOTES
 *Richtfunk* wird vor allem in den Weitverkehrsnetzen der Telekommunikationsanbieter
  genutzt, aber auch zur Anbindung in schwer zugänglichen Gebieten.
#+END_NOTES

  
*** Satellitenkommunikation

  Herstellung einer bidirektionalen Telekommunikation zwischen zwei Bodenstationen.
  
  - Vorteil:
    - Theoretische überall nutzbar.
    - Hohe Bandbreite.
    - Relativ hohe Abhör- und Störsicherheit.
    - Theoretisch unbegrenzte Reichweite.
  - Nachteile:
    - Direkte Verbindung zum Satelliten muss aufrecht erhalten werden:
      - geostationäre oder sehr viele Satelliten notwendig.
    - Hohe Kosten bei der Herstellung und Positionierung der Satelliten. 

*** Infrarot

  Es gibt im verschiedene Ansätze die mit Infrarotsignalen Daten übertragen:

  - *IrDA*: Datenaustausch zwischen Geräten auf kurzer Distanz. 
    - Vorteile:
      - vergleichsweise große Bandbreite.
      - Hohe Stör- und Abhörsicherheit.
    - Nachteile:
      - Sichtverbindung notwendig.
  - *Optische Freiraumkommunikation (FSO)*: Datenübertragung per (Infrarot-)Laser.
    - Vorteile:
      - Sehr hohe Bandbreite.
      - Reichweite bis zu einigen Kilometern.
    - Nachteile:
      - Sichtverbindung notwendig.
      - Störanfällig vor allem durch Streuung (Nebel).

* Footnotes

[fn:2] Die angegebene theoretische Übertragungsrate entspricht der Übertragungsrate, die in der Regel mit kaufbaren Geräten möglich ist.
Berücksichtigt sind dabei zwei Antennen und eine Kanalbreite von 80 MHz im Frequenzbereich von 5 GHz. Je nach Ausstattung kann der Wert
dieser theoretischen Übertragungsrate auch darüber oder darunter liegen.
[fn:1] Die angegebene maximal Übertragungsrate entspricht dem rechnerischen Maximum der theoretischen Übertragungsrate unter
Einbeziehung aller Leistungsmerkmale, die im jeweiligen Standard vorgesehen sind. In der praktischen Implementierung gibt es allerdings
Einschränkungen, wegen denen diese Übertragungsrate nicht realisierbar ist. Zum Vergleich der WLAN-Standards eignet sich deshalb eine
theoretische Übertragungsrate besser, die näher an der WLAN-Ausstattung in der Praxis ist.
