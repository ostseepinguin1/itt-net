Certainly! Below is the README file in Markdown format, followed by the content for the GPLv3 LICENSE file.

README.md
markdown
Copy code
# IPv4 Network Tools

This repository contains two Python programs for working with IPv4 addresses: `IPv4Test.py` and `IPv4Subnetting.py`. Below are the steps to install the prerequisites and run the programs.

## Prerequisites

Before running the programs, you need to install some Python packages. Make sure you have Python 3.x installed on your system. If not, you can download it from [Python's official website](https://www.python.org/downloads/).

### Installing Python Packages

To install the required Python packages, you can use the following commands:

#### For Kivy and KivyMD:

```bash
pip install kivy
pip install kivymd
```

For other Python standard libraries:
The following libraries are standard Python libraries and should be available by default:

- random
- ipaddress
- math
- yaml

If for some reason they are not, you can install them using pip:

```bash
pip install PyYAML
```

# Running the Programs
## Running IPv4Test

To run IPv4Test.py, navigate to the project directory and execute the following command:

```bash
python IPv4Test.py
```

## Running IPv4Subnetting

To run IPv4Subnetting.py, navigate to the project directory and execute the following command:

```bash
python IPv4Subnetting.py
```

# Troubleshooting

If you encounter any issues, make sure:

- You have Python 3.x installed.
- All required packages are installed.
- You are in the correct directory when running the commands.

# Contributing

If you would like to contribute to this project, please fork the repository and submit a pull request.

# License
This project is licensed under the GNU General Public License v3.0. See the LICENSE file for details.