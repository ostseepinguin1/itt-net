# IPv4 Netzwerk-Tools

Dieses Repository enthält zwei Python-Programme zur Arbeit mit IPv4-Adressen: `IPv4Test.py` und `IPv4Subnetting.py`. Im Folgenden finden Sie die Schritte zur Installation der Voraussetzungen und zum Ausführen der Programme.

## Voraussetzungen

Bevor Sie die Programme ausführen, müssen einige Python-Pakete installiert werden. Stellen Sie sicher, dass Python 3.x auf Ihrem System installiert ist. Falls nicht, können Sie es von der [offiziellen Python-Website](https://www.python.org/downloads/) herunterladen.

### Installation der Python-Pakete

Zur Installation der erforderlichen Python-Pakete können Sie die folgenden Befehle verwenden:

#### Für Kivy und KivyMD:

```bash
pip install kivy
pip install kivymd
```

Für andere Python-Standardbibliotheken:
Die folgenden Bibliotheken sind Standard-Python-Bibliotheken und sollten standardmäßig verfügbar sein:

- random
- ipaddress
- math
- yaml

Falls sie aus irgendeinem Grund nicht vorhanden sind, können Sie sie mit pip installieren:

```bash
pip install PyYAML
```

# Ausführen der Programme
### Ausführen von IPv4Test

Um IPv4Test.py auszuführen, navigieren Sie zum Projektverzeichnis und führen Sie den folgenden Befehl aus:

```bash
python IPv4Test.py
```

# Ausführen von IPv4Subnetting

Um IPv4Subnetting.py auszuführen, navigieren Sie zum Projektverzeichnis und führen Sie den folgenden Befehl aus:

```bash
python IPv4Subnetting.py
```

# Fehlerbehebung
Wenn Sie auf Probleme stoßen, stellen Sie sicher, dass:

- Python 3.x installiert ist.
- Alle erforderlichen Pakete installiert sind.

Sie sich im richtigen Verzeichnis befinden, wenn Sie die Befehle ausführen.

# Mitwirken

Wenn Sie an diesem Projekt mitwirken möchten, forken Sie das Repository und reichen Sie einen Pull-Request ein.

# Lizenz

Dieses Projekt steht unter der GNU General Public License v3.0. Einzelheiten finden Sie in der Datei LICENSE.