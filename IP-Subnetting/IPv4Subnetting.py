from kivy import Config
Config.set('graphics', 'multisamples', '0')
import os
os.environ['KIVY_GL_BACKEND'] = 'angle_sdl2'

from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRaisedButton
from kivymd.uix.textfield import MDTextField
from kivymd.uix.label import MDLabel
from kivymd.uix.selectioncontrol import MDSwitch
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
import random
import ipaddress
import math
import yaml

import sys

file_path = sys.argv[0]

working_directory = os.path.dirname(file_path)

os.chdir(working_directory)


if os.path.exists(file_path):
    # File exists, you can proceed with loading the YAML data
    print("File exists")
else:
    # File does not exist, handle the error accordingly
    print("File does not exist")


# Load explanations from YAML file
with open("explanations.yaml", "r") as stream:
    try:
        explanations = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


class SubnettingApp(MDApp):
    def build(self):
        self.title = "IPv4 Subnetting Übung"
        self.theme_cls.theme_style = "Dark"
        self.layout = MDBoxLayout(orientation="vertical")

        self.difficulty = "easy"  # oder "hard"
        self.difficulty_switch = MDSwitch()
        self.difficulty_switch.bind(active=self.on_switch_active)
        self.switch_label = MDLabel(text="Schwierig")
        self.switch_layout = MDBoxLayout()
        self.switch_layout.add_widget(self.switch_label)
        self.switch_layout.add_widget(self.difficulty_switch)

        self.result_label = MDLabel(
            size_hint_y=None, height=10 * 20
        )  # 20 pixels per line
        self.result_label.bind(texture_size=self.result_label.setter("size"))
        self.result_scroll = ScrollView(
            size_hint=(1, None), height=10 * 20
        )  # 20 pixels per line
        self.prefixlen = random.choice([8, 16, 24])
        self.result_scroll.add_widget(self.result_label)
        self.new_task()
        return self.layout

    def new_task(self, instance=None):
        # Entferne alle Widgets, einschließlich result_scroll
        for widget in list(self.layout.children):
            self.layout.remove_widget(widget)

        self.layout.add_widget(self.switch_layout)

        # Generiere eine neue Aufgabe und füge die Widgets wieder hinzu
        self.new_task_logic()

        # Füge result_scroll wieder am Ende hinzu
        self.result_label.text = ""
        self.layout.add_widget(self.result_scroll)

    def new_task_logic(self):
        # Generieren Sie eine neue Aufgabe
        ip = f"{random.randint(1, 223)}.{random.randint(0, 255)}.{random.randint(0, 255)}.{random.randint(0, 255)}"

        if self.difficulty == "hard":
            prefixlen = random.randint(8, 28)
        else:
            prefixlen = random.choice([8, 16, 24])

        self.network = ipaddress.IPv4Network(f"{ip}/{prefixlen}", strict=False)
        self.num_subnets = random.randint(2, 8)

        # Erstellen Sie ein neues Label für die Aufgabe
        task_label = MDLabel(
            text=f"Netzwerk: {self.network}, Anzahl der Subnetze: {self.num_subnets}",
            theme_text_color="Custom",
            text_color=(1, 1, 1, 1),
            font_style="H5",
            bold=True,
            halign="center",
        )
        task_label.font_size = 24 * 1.2  # 120% of 24sp

        # Fügen Sie das neue Label und die neuen Eingabefelder hinzu
        self.layout.add_widget(task_label)
        self.add_entries()

    def add_entries(self):
        self.entries = {}
        self.entry_list = []
        entry_labels = [
            "Subnetz CIDR",
            "Blocksize",
            "Dezimale Subnetzmaske",
            "Maximale Anzahl von Hosts",
            "Erste Subnetzadresse",
            "Zweite Subnetzadresse",
            "Letzte Subnetzadresse",
        ]
        for label_text in entry_labels:
            row_layout = MDBoxLayout()
            row_layout.add_widget(MDLabel(text=f"{label_text}:"))
            entry = MDTextField(input_type="text")
            entry.bind(focus=self.on_focus)
            self.entry_list.append(entry)  # Add entry to the list
            self.entries[label_text] = entry
            row_layout.add_widget(entry)
            self.layout.add_widget(row_layout)

        button_layout = MDBoxLayout(spacing=10)
        check_button = MDRaisedButton(text="Prüfen")
        check_button.bind(on_press=self.check_values)
        button_layout.add_widget(check_button)

        new_task_button = MDRaisedButton(text="Neue Aufgabe")
        new_task_button.bind(on_press=self.new_task)
        button_layout.add_widget(new_task_button)

        self.layout.add_widget(button_layout)

    def on_focus(self, instance, value):
        if value:
            self.current_index = self.entry_list.index(instance)
            Window.bind(on_key_down=self.on_key_down)

    def on_key_down(self, instance, keyboard, keycode, text, modifiers):
        if keycode == 40:  # 40 is the keycode for the Tab key
            next_index = (self.current_index + 1) % len(self.entry_list)
            self.entry_list[next_index].focus = True
            return True  # Consume the event to prevent default behavior
        return False  # Continue with the default behavior otherwise

    def on_switch_active(self, instance, value):
        if value:
            self.difficulty = "hard"
        else:
            self.difficulty = "easy"

    def calculate_values(self):
        zusaetzliche_bits = math.ceil(math.log2(self.num_subnets))
        subnetz_cidr = self.network.prefixlen + zusaetzliche_bits
        blocksize = 2 ** (8 - (subnetz_cidr % 8))
        if blocksize == 256:
            blocksize = 1
        subnets = list(self.network.subnets(new_prefix=subnetz_cidr))
        subnetz_maske = subnets[0].netmask
        max_hosts = subnets[0].num_addresses - 2
        erstes_subnetz = subnets[0].network_address
        zweites_subnetz = subnets[1].network_address
        letztes_subnetz = subnets[-1].network_address
        return (
            subnetz_cidr,
            blocksize,
            str(subnetz_maske),
            max_hosts,
            str(erstes_subnetz),
            str(zweites_subnetz),
            str(letztes_subnetz),
        )

    def check_values(self, instance):
        try:
            benutzer_subnetz_cidr = int(self.entries["Subnetz CIDR"].text)
            benutzer_blocksize = int(self.entries["Blocksize"].text)
            benutzer_subnetz_maske = self.entries["Dezimale Subnetzmaske"].text
            benutzer_max_hosts = int(self.entries["Maximale Anzahl von Hosts"].text)
            benutzer_erstes_subnetz = self.entries["Erste Subnetzadresse"].text
            benutzer_zweites_subnetz = self.entries["Zweite Subnetzadresse"].text
            benutzer_letztes_subnetz = self.entries["Letzte Subnetzadresse"].text
        except ValueError:
            self.result_label.text = "Bitte alle Felder ausfüllen."
            return

        (
            subnetz_cidr,
            blocksize,
            subnetz_maske,
            max_hosts,
            erstes_subnetz,
            zweites_subnetz,
            letztes_subnetz,
        ) = self.calculate_values()

        self.result_label.text = ""
        fehler = 0

        def check_value(value_name, user_value, correct_value, explanation_key):
            nonlocal fehler
            if user_value != correct_value:
                self.result_label.text += (
                    f"Falsche {value_name}. Korrekt ist {correct_value}. "
                    f"{explanations[explanation_key]}\n==============================\n"
                )
                fehler += 1

        check_value("Subnetz-CIDR", benutzer_subnetz_cidr, subnetz_cidr, "cidr")
        check_value("Blocksize", benutzer_blocksize, blocksize, "blocksize")
        check_value("Subnetzmaske", benutzer_subnetz_maske, subnetz_maske, "netmask")
        check_value(
            "Maximale Anzahl von Hosts", benutzer_max_hosts, max_hosts, "num_hosts"
        )
        check_value(
            "Erste Subnetzadresse",
            benutzer_erstes_subnetz,
            erstes_subnetz,
            "first_subnet",
        )
        check_value(
            "Zweite Subnetzadresse",
            benutzer_zweites_subnetz,
            zweites_subnetz,
            "second_subnet",
        )
        check_value(
            "Letzte Subnetzadresse",
            benutzer_letztes_subnetz,
            letztes_subnetz,
            "last_subnet",
        )

        if fehler == 0:
            self.result_label.text += "Alles ist korrekt! Gut gemacht! 🌟"
        else:
            self.result_label.text += (
                "\nKeine Sorge, jeder macht Fehler. Versuche es erneut! 💪"
            )


if __name__ == "__main__":
    SubnettingApp().run()
