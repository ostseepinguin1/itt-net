from kivy import Config
Config.set('graphics', 'multisamples', '0')
import os
os.environ['KIVY_GL_BACKEND'] = 'angle_sdl2'

from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRaisedButton
from kivymd.uix.textfield import MDTextField
from kivymd.uix.label import MDLabel
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.widget import Widget
import random
import ipaddress
import yaml
from typing import Dict, Tuple, Union, List, Any
import sys

file_path = sys.argv[0]

working_directory = os.path.dirname(file_path)

os.chdir(working_directory)


if os.path.exists(file_path):
    # File exists, you can proceed with loading the YAML data
    print("File exists")
else:
    # File does not exist, handle the error accordingly
    print("File does not exist")


# Laden der Erklärungen aus einer YAML-Datei
with open("explanations.yaml", "r") as stream:
    try:
        explanations = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


class IPCheckerApp(MDApp):
    """Eine Kivy-App, die eine IP-Adressprüfung durchführt."""

    def build(self) -> MDBoxLayout:
        """
        Erstellt die Benutzeroberfläche der App.

        Returns:
            MDBoxLayout: Das Hauptlayout der App.
        """
        self.title = "IP-Adressprüfer"

        # Erstellt ein vertikales Box-Layout
        self.layout = MDBoxLayout(orientation="vertical")

        # Setzt den Theme-Stil auf Dunkel
        self.theme_cls.theme_style = "Dark"

        # Erstellt ein Label für die Ergebnisse
        self.result_label = MDLabel(size_hint_y=None, height=10 * 20)

        # Bindet die Texturgröße an die Größe des Labels
        self.result_label.bind(texture_size=self.result_label.setter("size"))

        # Erstellt einen Scrollbereich für das Ergebnis-Label
        self.result_scroll = ScrollView(size_hint=(1, None), height=10 * 20)
        self.result_scroll.add_widget(self.result_label)

        # Generiert eine neue Aufgabe (diese Methode muss noch implementiert werden)
        self.generate_new_task()

        # Fügt den Scrollbereich zum Hauptlayout hinzu
        self.layout.add_widget(self.result_scroll)

        return self.layout

    def generate_new_task(self) -> None:
        """
        Generiert eine neue IP-Adresse und ein neues Subnetz.
        Aktualisiert das GUI-Label und fügt Eingabefelder hinzu.
        """
        # Generiert eine zufällige IP-Adresse und Subnetzmaske
        ip = f"{random.randint(1, 223)}.{random.randint(0, 255)}.{random.randint(0, 255)}.{random.randint(0, 255)}"
        prefixlen = random.randint(8, 30)
        self.ip_interface = f"{ip}/{prefixlen}"

        # Erstellt ein Label mit der generierten IP-Adresse und Subnetzmaske
        ip_label = MDLabel(
            text=f"Generierte IP und Subnetz: {self.ip_interface}",
            theme_text_color="Custom",
            text_color=(1, 1, 1, 1),  # Weiß im RGBA-Format
            font_style="H5",  # Entspricht einer Schriftgröße von 24sp
            bold=True,
            halign="center",
        )

        # Fügt das Label zum Hauptlayout hinzu
        self.layout.add_widget(ip_label)

        # Fügt Eingabefelder hinzu (diese Methode muss noch implementiert werden)
        self.add_entries()

    def add_entries(self) -> None:
        """
        Fügt Eingabefelder für verschiedene Netzwerkinformationen zur Benutzeroberfläche hinzu.
        Erstellt auch Schaltflächen für die Überprüfung der Werte und für das Generieren einer neuen Aufgabe.
        """
        self.entries = {}
        entry_list = []

        # Erstellt Eingabefelder für verschiedene Netzwerkinformationen
        for label_text in [
            "Blocksize",
            "Netzwerkadresse",
            "Netzwerkmaske",
            "Anzahl von IPs",
            "Anzahl von Hosts",
            "Erster Host",
            "Letzter Host",
            "Broadcast-Adresse",
        ]:
            row_layout = MDBoxLayout()
            row_layout.add_widget(MDLabel(text=f"{label_text}:"))
            entry = MDTextField(input_type="text")
            entry_list.append(entry)
            self.entries[label_text] = entry
            row_layout.add_widget(entry)
            self.layout.add_widget(row_layout)

        # Setzt die Tab-Navigation auf
        for i, entry in enumerate(entry_list):
            entry.bind(focus=self.on_focus)
        self.entry_list = entry_list  # Speichert die Liste für die spätere Verwendung

        # Erstellt Schaltflächen für die Überprüfung und für eine neue Aufgabe
        button_layout = MDBoxLayout(spacing=20)
        check_button = MDRaisedButton(text="Prüfen")
        check_button.bind(
            on_press=self.check_values
        )  # Diese Methode muss noch implementiert werden
        button_layout.add_widget(check_button)

        new_task_button = MDRaisedButton(text="Neue Aufgabe")
        new_task_button.bind(
            on_press=self.new_task
        )  # Diese Methode muss noch implementiert werden
        button_layout.add_widget(new_task_button)

        # Fügt das Schaltflächen-Layout zum Hauptlayout hinzu
        self.layout.add_widget(button_layout)

    def on_focus(self, instance: Widget, value: bool) -> None:
        """
        Wird aufgerufen, wenn ein Eingabefeld den Fokus erhält oder verliert.

        Args:
            instance (Widget): Das Eingabefeld-Widget.
            value (bool): True, wenn das Eingabefeld den Fokus erhält; False, wenn es den Fokus verliert.
        """
        if value:  # Nur berücksichtigen, wenn ein Textfeld den Fokus erhält
            self.current_index = self.entry_list.index(
                instance
            )  # Aktuellen Index aktualisieren
            Window.bind(on_key_down=self.on_key_down)

    def on_key_down(
        self,
        instance: Any,
        keyboard: Any,
        keycode: int,
        text: str,
        modifiers: List[str],
    ) -> bool:
        """
        Wird aufgerufen, wenn eine Taste gedrückt wird, während ein Eingabefeld den Fokus hat.

        Args:
            instance (Any): Das Eingabefeld-Widget oder ein anderes Objekt.
            keyboard (Any): Das Tastatur-Objekt.
            keycode (int): Der KeyCode der gedrückten Taste.
            text (str): Der Text der gedrückten Taste.
            modifiers (List[str]): Eine Liste von Modifikatoren (z. B. 'shift', 'ctrl').

        Returns:
            bool: True, wenn das Ereignis verbraucht wurde; False, wenn die Standardverhalten fortgesetzt werden soll.
        """
        if keycode == 40:  # 40 ist der KeyCode für die Tab-Taste
            next_index = (self.current_index + 1) % len(self.entry_list)
            self.entry_list[next_index].focus = True
            return (
                True  # Verbraucht das Ereignis, um das Standardverhalten zu verhindern
            )
        return False  # Setzt das Standardverhalten sonst fort

    def new_task(self, instance: Widget) -> None:
        """
        Erstellt eine neue Aufgabe, indem alle Widgets entfernt und dann wieder hinzugefügt werden.

        Args:
            instance (Widget): Das Widget, das das Ereignis ausgelöst hat (in diesem Fall der Button für die neue Aufgabe).
        """
        # Entfernt alle Widgets, einschließlich result_scroll
        for widget in list(self.layout.children):
            self.layout.remove_widget(widget)

        # Generiert eine neue Aufgabe und fügt die Widgets wieder hinzu
        self.generate_new_task()

        # Fügt result_scroll wieder am Ende hinzu
        self.layout.add_widget(self.result_scroll)

    def check_values(self, instance: Widget) -> None:
        """
        Überprüft die vom Benutzer eingegebenen Werte und aktualisiert das Ergebnis-Label.

        Args:
            instance (Widget): Das Widget, das das Ereignis ausgelöst hat (in diesem Fall der Prüfbutton).
        """
        try:
            # Sammelt die Benutzerwerte aus den Eingabefeldern
            user_values = {
                key: int(entry.text) if entry.text.isdigit() else entry.text
                for key, entry in self.entries.items()
            }
        except ValueError:
            self.result_label.text = "Bitte alle Felder ausfüllen."
            return

        # Berechnet die korrekten Werte basierend auf der generierten IP-Adresse und dem Subnetz
        (
            blocksize,
            network_address,
            netmask,
            num_ips,
            num_hosts,
            first_host,
            last_host,
            broadcast,
        ) = self.calculate_values(
            self.ip_interface
        )  # Diese Methode muss noch implementiert werden

        result = ""
        errors = 0

        # Überprüft die Benutzerwerte gegen die korrekten Werte
        for label_text, correct_value, explanation_key in [
            ("Blocksize", blocksize, "blocksize"),
            ("Netzwerkadresse", network_address, "network_address"),
            ("Netzwerkmaske", netmask, "netmask"),
            ("Anzahl von IPs", num_ips, "num_ips"),
            ("Anzahl von Hosts", num_hosts, "num_hosts"),
            ("Erster Host", first_host, "first_host"),
            ("Letzter Host", last_host, "last_host"),
            ("Broadcast-Adresse", broadcast, "broadcast"),
        ]:
            user_value = user_values[label_text]
            if user_value != correct_value:
                result += f"Falsche {label_text}. Korrekt ist {correct_value}\n--------------------------\n"
                result += f"Erklärung: {explanations[explanation_key]}\n=======================\n"
                errors += 1

        # Aktualisiert das Ergebnis-Label
        if errors == 0:
            result += "Alles ist korrekt! Gut gemacht! 🌟"
        else:
            result += (
                "\nMach dir keine Sorgen, jeder macht Fehler. Versuch es erneut! 💪"
            )

        self.result_label.text = result

    def calculate_values(
        self, ip_interface: str
    ) -> Tuple[int, str, str, int, int, Union[str, None], Union[str, None], str]:
        """
        Berechnet verschiedene Netzwerkinformationen basierend auf einer gegebenen IP-Adresse und Subnetzmaske.

        Args:
            ip_interface (str): Die IP-Adresse und Subnetzmaske im Format "IP/Prefixlänge", z.B. "192.168.1.1/24".

        Returns:
            Tuple: Ein Tupel, das verschiedene Netzwerkinformationen enthält:
                - Blockgröße
                - Netzwerkadresse
                - Netzwerkmaske
                - Anzahl der IPs im Netzwerk
                - Anzahl der Hosts im Netzwerk
                - Erste Host-Adresse im Netzwerk
                - Letzte Host-Adresse im Netzwerk
                - Broadcast-Adresse
        """
        network = ipaddress.IPv4Network(ip_interface, strict=False)
        blocksize = 2 ** (8 - (network.prefixlen % 8))
        if blocksize == 256:
            blocksize = 1
        network_address = str(network.network_address)
        netmask = str(network.netmask)
        num_ips = network.num_addresses
        num_hosts = num_ips - 2 if num_ips > 2 else 0
        first_host = str(network.network_address + 1) if num_hosts > 0 else None
        last_host = str(network.broadcast_address - 1) if num_hosts > 0 else None
        broadcast = str(network.broadcast_address)

        return (
            blocksize,
            network_address,
            netmask,
            num_ips,
            num_hosts,
            first_host,
            last_host,
            broadcast,
        )


if __name__ == "__main__":
    """
    Startet die IPCheckerApp, wenn das Skript direkt ausgeführt wird.
    """
    IPCheckerApp().run()
